# MyLibrary

## Auteur
	- Nom: NOUZILLAT
	- Prénom: Alix
	- Métier: Concepteur Développeur

## Description
Projet de bibliothèque

## Installation
Lancer le script **install.sh**

## Désinstallation
Lancer le script **uninstall.sh**


## Collaborateurs
	- Nom : CHALMEL
	- Prénom : Vincent
	- Métier : CDO
	- Contribution apportée : Pas ouf...
